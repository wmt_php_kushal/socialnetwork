<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>['web','auth']],function (){
//    Auth::routes();

    Route::post('/dashboard','PostController@postCreatePost');
    Route::get('/dashboard','PostController@getDashboard')->name('dashboard');
    Route::get('/delete/{id}','PostController@getDeletePost')->name('delete');
    Route::get('/edit/{id}','PostController@edit');
    Route::put('edit/{id}','PostController@editPost');
    Route::get('account/','UserController@getAccount')->name('account');
    Route::put('/updateaccount','UserController@saveAccount')->name('account.save');
    Route::get('/userimage/{filename}','UserController@getUserImage')->name('account.image');
    Route::post('/like','PostController@likePost')->name('like');
});
