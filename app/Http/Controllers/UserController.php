<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\File;
//use http\Env\Response;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    public function getAccount(){
        return view('account',['user'=> Auth::user()]);
    }
    public function saveAccount(Request $request){
        try {
            $this->validate($request, [
                'name' => 'required'
            ]);
        } catch (ValidationException $e) {
        }
        $user= Auth::user();
        $user->name=$request['name'];
        $user->update();
        $file = $request->file('image');
        $filename= $request['name'].'-'.$user->id.'.jpg';
        if($file){
//            dd($file);
            Storage::disk('local')->put($filename, File::get($file));
        }
        return redirect()->route('account');
    }
    public function getUserImage($filename){
//        dd('2');
        $file=Storage::disk('local')->get($filename);
        return new Response($file,200);
    }
}
