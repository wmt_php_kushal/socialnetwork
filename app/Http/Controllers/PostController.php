<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function getDashboard(){
        $posts=Post::orderBy('created_at','desc')->get();
        return view('dashboard',['posts'=>$posts]);
    }
    public function postCreatePost(Request $request){

        $this->validate($request,[
            'body'=> 'required|max:1000'
        ]);
        $post= new Post();
        $post->body=$request->body;
        $message="There was an error!!!";
        if($request->user()->posts()->save($post)){
            $message="Post Succesfully Created...";
        }
        return redirect()->route('dashboard')->with(['message'=>$message]);
    }
    public function getDeletePost($id){
        $post=Post::find($id);
        if(Auth::user()!=$post->user){
            return redirect()->back();
        }
        $post->delete();
        return redirect()->route('dashboard')->with(['message'=>'Deleted Post Successfully']);
    }
    public function edit($id){
        $posts=Post::find($id);
        return view('edit',['posts'=>$posts]);
    }
    public function editPost(Request $request,$id){

        $this->validate($request,[
            'body'=> 'required|max:1000'
        ]);

        $post=Post::find($id);

        if(Auth::user()!=$post->user){
            return redirect()->back();
        }
        $post->body=$request->body;

        if($request->user()->posts()->save($post)){
            $message="Post Succesfully Updated...";
        }
        return redirect()->route('dashboard')->with(['message'=>$message,'posts'=>$post]);

    }
    public function likePost(Request $request){
        $post_id=$request['postid'];
        $is_like=$request['islike']==='true';

    }
}
