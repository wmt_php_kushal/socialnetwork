@extends('home')

@section('content')
    @include('messages')
    <div class="container">
        <scetion class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <header>
                    <h3>
                        Dashboard!
                    </h3>
                </header>
                <form action="{{ route('dashboard') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <textarea class="form-control" name="body" id="body" rows="5" placeholder="Posts"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Create Post</button>
                  </form>
            </div>
        </scetion>
        <section class="row posts">
            <div class="col-md-6 col-md-offset-3">
                <header></header>
                @foreach($posts as $p)
                    <article class="post">
                        <p>{{$p->body}}
                        </p>
                        <p>Author:<strong>{{$p->user->name}}</strong></p>
                        <div class="info">
                            <p>Some Kind of Data</p>
                            <a href="#" class="like" id="liked">Like</a>
                            <a href="#" class="like" id="disliked">Dislike</a>
                            @if(Auth::user()==$p->user)
                                |
                                <form action="{{ route('dashboard') }}" method="POST">
                                    @csrf
                                    @method('put')
                                    <div class="form-group">
                                        <a href="/edit/{{$p->id}}">Edit</a>
                                    </div>
                                </form>
                                    <a href="/delete/{{$p->id}}">Delete</a>
                            @endif
                        </div>
                    </article>
                @endforeach

            </div>
        </section>
    </div>
    <script>
        var urlLike='{{route('like')}}';
        $('.like').on('click',function(event){
            event.preventDefault();
            postId=event.target.parentNode.parentNode.dataset['postid'];
            let isLike=Event.target.previousElement ==null;
            $.ajax({
                method:'POST',
                url:urlLike,
                data:{isLIke:isLike,postId:postId, _token:token}
            });
            .done(function(){

            })
        });

    </script>
@endsection
