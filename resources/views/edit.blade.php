@extends('home')

@section('content')
    @include('messages')
    <div class="container">
        <scetion class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <header>
                    <h3>
                        Dashboard!
                    </h3>
                </header>
                <form action="{{$posts->id }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <textarea class="form-control" name="body" id="body" rows="5" placeholder="Posts">{{$posts->body}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Update Post</button>
                </form>
            </div>
        </scetion>
@endsection
