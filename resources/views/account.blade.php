@extends('layouts.app')

@section('title')
    Account
@endsection

@section('content')
    <div class="container">
        <section class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <header><h3>Your Account</h3></header>
                <form action="{{ route('account.save') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $user->name }}" id="first_name">
                    </div>
                    <div class="form-group">
                        <label for="image">Image (only .jpg)</label>
                        <input type="file" name="image" class="form-control" id="image">
                    </div>
                    <button type="submit" class="btn btn-primary">Save Account</button>
                </form>
                @if (Storage::disk('local')->has($user->name . '-' . $user->id . '.jpg'))
                    <section class="row new-post">
                        <div class="col-md-6 col-md-offset-3">
                            <img src="{{ route('account.image', ['filename' => $user->name . '-' . $user->id . '.jpg']) }}" alt="Image" >
                        </div>
                    </section>
                @endif
            </div>
        </section>
    </div>

@endsection
